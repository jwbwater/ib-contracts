# Interactive Brokers Contracts


## Summary

A list of contracts available through IB's API which was obtained by numerous calls to
[reqMatchingSymbols](https://interactivebrokers.github.io/tws-api/classIBApi_1_1EClient.html#aa0bff193c5cbaa73e89dccdd0f027195),
supplemented with human readable names, the long\_name field from calling
[reqContractDetails](https://interactivebrokers.github.io/tws-api/classIBApi_1_1EClient.html#ade440c6db838b548594981d800ea5ca9)
for each contract.

These contracts were inserted into a postgres database table named contract. The file in this repo
is an SQL file obtained from this table using the following postgres command:

```
pg_dump -t contract -U db_owner db_name > contracts.sql
```

After editing contracts.sql to change db\_owner, the database owner's name, to your desired name,
you can load this file into a postgres table with this command:

```
psql db_name < contracts.sql
```

Similar commands should work for other SQL databases. 

## Contract Statistics

### Number of contracts in the table

```
ibapi=# select count(*) from contract;

 count
--------
 113728
(1 row)
```

### Number of contracts by security type

```
 sec_type | count
----------+-------
 CASH     |   115
 CMDTY    |     4
 CRYPTO   |     8
 FUND     | 23363
 IND      |  4550
 STK      | 85688
(6 rows)
```

### Number of contracts by primary exchange

```
 primary_exchange | count
------------------+-------
 AEB              |   402
 AEQLIT           |   234
 ALLFUNDS         | 10501
 AMEX             |   515
 AQS              |     1
 ARCA             |  1978
 ASX              |  2691
 ATH              |    22
 BATS             |   620
 BELFOX           |     3
 BM               |   235
 BOVESPA          |   243
 BSEI             |     1
 BUX              |    40
 BVL              |    41
 BVME             |   459
 BVME.ETF         |   728
 CARISK           |    13
 CBOE             |   218
 CBOE2            |     2
 CBOT             |    37
 CDE              |     8
 CFE              |     2
 CHINEXT          |   474
 CHIXAU           |    27
 CME              |   412
 COMEX            |     3
 CORPACT          | 10066
 CPH              |    75
 DOLLR4LOT        |   352
 EBS              |  1854
 ECBOT            |    87
 ENDEX            |     2
 ENEXT.BE         |   138
 EUREX            |   165
 EURONEXT         |   257
 FTA              |    15
 FUNDSERV         | 12425
 FUNDSETL         |   180
 FWB              |  1122
 FWB2             |  3128
 GETTEX           |   174
 GETTEX2          |   175
 HEX              |    79
 HKFE             |   104
 IBCMDTY          |     3
 IBIS             |  1184
 IBIS2            |   648
 IBKRAM           |    11
 IBMETAL          |     1
 ICEEU            |    47
 ICEEUSOFT        |     4
 IDEALPRO         |   115
 IDEM             |     7
 IPE              |    33
 IR               |   398
 ISE              |    35
 ISED             |    16
 JSE              |   120
 KSE              |    16
 LSE              |  2249
 LSEETF           |  2645
 LSEIOB1          |    76
 MATIF            |     7
 MEFFRV           |    21
 MEXI             |  2637
 MOEX             |   132
 MONEP            |    19
 NASDAQ           |  5278
 N.RIGA           |    19
 NSE              |  2282
 N.TALLINN        |    36
 N.VILNIUS        |    29
 NYBOT            |    32
 NYMEX            |    70
 NYSE             |  3377
 NYSELIFFE        |     9
 OMS              |     6
 OMXNO            |     4
 OSE              |   315
 OSE.JPN          |    64
 OTCBB            |    41
 PAXOS            |     8
 PHLX             |    68
 PINK             |  9466
 PRA              |    15
 PSE              |  1620
 PURE             |   775
 RUSSELL          |    53
 SBF              |  1319
 SEHK             |  2786
 SEHKNTL          |  1029
 SEHKSTAR         |    58
 SEHKSZSE         |   972
 SFB              |  1085
 SGX              |   777
 SNFE             |    11
 SWB              |   200
 SWB2             |   301
 TASE             |   883
 TSE              |  2289
 TSEJ             |  4219
 VALUE            | 10791
 VENTURE          |  1930
 VSE              |    90
 WSE              |   689
(106 rows)
```
